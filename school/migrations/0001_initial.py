# Generated by Django 4.2.4 on 2023-09-03 17:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='название')),
                ('preview', models.ImageField(blank=True, null=True, upload_to='courses/', verbose_name='превью')),
                ('description', models.TextField(verbose_name='описание')),
                ('price', models.IntegerField(default=0, verbose_name='цена курса')),
            ],
        ),
        migrations.CreateModel(
            name='Lesson',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='название')),
                ('preview', models.ImageField(blank=True, null=True, upload_to='lessons/', verbose_name='превью')),
                ('description', models.TextField(verbose_name='описание')),
                ('link_to_video', models.CharField(max_length=150, verbose_name='ссылка на видео')),
            ],
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('payment_date', models.DateField(verbose_name='дата оплаты')),
                ('amount', models.IntegerField(verbose_name='сумма оплаты')),
                ('payment_method', models.CharField(choices=[('cash', 'наличные'), ('bank_transfer', 'перевод на счет')], max_length=20, verbose_name='способ оплаты')),
                ('payment_intent_id', models.CharField(blank=True, max_length=10, null=True, verbose_name='id намерения платежа')),
                ('payment_method_id', models.CharField(blank=True, max_length=10, null=True, verbose_name='id метода платежа')),
                ('status', models.CharField(blank=True, max_length=50, null=True, verbose_name='cтатус платежа')),
                ('confirmation', models.BooleanField(default=False, verbose_name='подтверждение платежа')),
            ],
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.BooleanField(default=True, verbose_name='статус подписки')),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='school.course', verbose_name='курс')),
            ],
            options={
                'verbose_name': 'подписка',
                'verbose_name_plural': 'подписки',
            },
        ),
    ]
